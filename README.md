# CAP Coding Standard (CAPCS)

Provides the `CAPCS` ruleset for `phpcs`. Based on slevomat/coding-standard, which must be pinned
to a specific version because deprecations are a breaking change for us.

## Local Installation

Install locally using composer:

```
composer require --dev capuk/coding-standard
```

### Configure PHPCS

*The instructions below assume that composer's `bin-dir` is set to `bin`. If
yours isn't then change the `phpcs` path from `bin/phpcs` to `vendor/bin/phpcs`.*

Create `phpcs.xml` in the project root, containing:

    <?xml version="1.0"?>
    <ruleset>
        <config name="installed_paths" value="../../capuk/coding-standard/src" />
        <rule ref="CAPCS" />
    </ruleset>

`phpcs` will now use the `CAPCS` ruleset by default, e.g:

    bin/phpcs src/example.php
    
Alternativley, `phpcs` can be configured from the command line:
 
    bin/phpcs --config-set installed_paths "../../capuk/coding-standard/src"
    bin/phpcs --config-set default_standard CAPCS
    
Note that the `phpcs` config file which stores these settings is located inside
the `phpcs` package directory and may be overwritten when composer updates the
package, so using `phpcs.xml` is a more reliable way to configure `phpcs`.

## Global Installation

Install globally using composer:
    
    composer global require capuk/coding-standard ^1.0

As for a local installation, `phpcs` can be configured from the command line:
 
    phpcs --config-set installed_paths "../../capuk/coding-standard/src"
    phpcs --config-set default_standard CAPCS
    
Or using the `phpcs.xml` file above, which can be in the project root *or any
parent directory*.

## Testing

From the project root:

    composer install
    // Run CAPCS against an example PHP file
    bin/phpcs src/invalid-example.php
