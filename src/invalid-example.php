<?php

use PHP_CodeSniffer\Tests\Core\Autoloader\B;
use function intersectionReturnTypes;

/**
  * @return int[]
*/
function test(): array
{
    $point = [
        "x" => 1,
        "y" => 2
    ];
    $point += [
        "z" => 3
    ];
    return $point;
}
