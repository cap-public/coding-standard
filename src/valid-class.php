<?php

declare(strict_types=1);

namespace CAPCS;

class MyClass
{
    /**
     * PHPCS doesn't read multiple files, so we should be allowed to use PHPDoc types without setting native types
     * because it causes PHP errors to use native types when a parent class doesn't use them.
     *
     * @var string $string
     */
    public $string;

    /**
     * Similarly for parameter types. Return types can be overwritten without causing PHP errors so we don't have
     * to worry about them.
     *
     * @param string $max
     */
    public function randomDiceRoll($max = 6): int
    {
        return $max - 2;
    }
}
