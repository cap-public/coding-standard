<?xml version="1.0"?>
<ruleset name="CAPCS">
    <description>CAP's PHP coding standard</description>

    <!-- Relative path from PHPCS source location, NOT relative to this file. -->
    <config name="installed_paths" value="../../slevomat/coding-standard"/>

    <rule ref="PSR1"/>
    <rule ref="PSR12">
        <exclude name="PSR12.Files.FileHeader.SpacingAfterBlock"/>
    </rule>

    <!---->
    <!-- Arrays -->
    <!---->

    <rule ref="Generic.Arrays.DisallowLongArraySyntax"/>

    <rule ref="SlevomatCodingStandard.Arrays.DisallowImplicitArrayCreation"/>
    <rule ref="SlevomatCodingStandard.Arrays.MultiLineArrayEndBracketPlacement"/>
    <rule ref="SlevomatCodingStandard.Arrays.SingleLineArrayWhitespace"/>
    <rule ref="SlevomatCodingStandard.Arrays.TrailingArrayComma"/>


    <!---->
    <!-- Classes & Interfaces -->
    <!---->

    <rule ref="SlevomatCodingStandard.Classes.ClassConstantVisibility"/>

    <!-- Enforce classes elements (uses, constants, methods, etc.) follow a given structure -->
    <rule ref="SlevomatCodingStandard.Classes.ClassStructure">
        <properties>
            <property name="groups" type="array">
                <element value="uses"/>

                <element value="enum cases"/>

                <element value="public constants"/>
                <element value="constants"/>

                <element value="public properties"/>
                <element value="properties"/>

                <element value="constructor"/>
                <element value="destructor"/>
                <element value="methods"/>
            </property>
        </properties>
    </rule>

    <!-- "self::CONSTANT" must be used instead of "static::CONSTANT" for non-static constants -->
    <rule ref="SlevomatCodingStandard.Classes.DisallowLateStaticBindingForConstants"/>

    <!-- Each constant or property definition should be on its own line -->
    <rule ref="SlevomatCodingStandard.Classes.DisallowMultiConstantDefinition"/>
    <rule ref="SlevomatCodingStandard.Classes.DisallowMultiPropertyDefinition"/>

    <rule ref="SlevomatCodingStandard.Classes.EmptyLinesAroundClassBraces">
        <properties>
            <property name="linesCountAfterOpeningBrace" value="0"/>
            <property name="linesCountBeforeClosingBrace" value="0"/>
        </properties>
    </rule>

    <!-- Class names should only be referenced using ::class -->
    <rule ref="SlevomatCodingStandard.Classes.ModernClassNameReference"/>

    <!-- Enforce spacing within property declarations and order of modifiers ("public static" -->
    <!-- not "static public") -->
    <rule ref="SlevomatCodingStandard.Classes.PropertyDeclaration"/>

    <rule ref="SlevomatCodingStandard.Classes.RequireSingleLineMethodSignature">
        <properties>
            <property name="maxLineLength" value="100"/>
        </properties>
    </rule>

    <!-- Spacing sniffs -->
    <rule ref="SlevomatCodingStandard.Classes.BackedEnumTypeSpacing"/>
    <rule ref="SlevomatCodingStandard.Classes.ClassMemberSpacing"/>
    <rule ref="SlevomatCodingStandard.Classes.ConstantSpacing"/>
    <rule ref="SlevomatCodingStandard.Classes.MethodSpacing"/>
    <rule ref="SlevomatCodingStandard.Classes.ParentCallSpacing"/>
    <rule ref="SlevomatCodingStandard.Classes.PropertySpacing"/>
    <rule ref="SlevomatCodingStandard.Classes.TraitUseSpacing">
        <properties>
            <property name="linesCountBeforeFirstUseWhenFirstInClass" value="0"/>
            <property name="linesCountAfterLastUseWhenLastInClass" value="0"/>
        </properties>
    </rule>

    <rule ref="SlevomatCodingStandard.Classes.SuperfluousAbstractClassNaming"/>
    <rule ref="SlevomatCodingStandard.Classes.SuperfluousErrorNaming"/>
    <rule ref="SlevomatCodingStandard.Classes.SuperfluousExceptionNaming"/>
    <rule ref="SlevomatCodingStandard.Classes.SuperfluousInterfaceNaming"/>
    <rule ref="SlevomatCodingStandard.Classes.SuperfluousTraitNaming"/>

    <!-- Each use must be on its own line -->
    <rule ref="SlevomatCodingStandard.Classes.TraitUseDeclaration"/>

    <rule ref="SlevomatCodingStandard.Classes.UselessLateStaticBinding"/>


    <!---->
    <!-- Comments -->
    <!---->

    <!-- Forbids use of PERL style comments (i.e. starting with '#') -->
    <rule ref="PEAR.Commenting.InlineComment"/>

    <!-- Checks for comments containing a high percentage of code -->
    <rule ref="Squiz.PHP.CommentedOutCode">
        <properties>
            <property name="maxPercentage" value="50"/>
        </properties>
    </rule>

    <!-- Deprecated annotations should have a description -->
    <rule ref="SlevomatCodingStandard.Commenting.DeprecatedAnnotationDeclaration"/>

    <!-- Disallow comments at the end of lines, e.g. "$x = 1; // set $x to 1" -->
    <rule ref="SlevomatCodingStandard.Commenting.DisallowCommentAfterCode"/>

    <rule ref="SlevomatCodingStandard.Commenting.DocCommentSpacing">
        <properties>
            <property name="linesCountBeforeFirstContent" value="0"/>
            <property name="linesCountAfterLastContent" value="0"/>
            <property name="linesCountBetweenDescriptionAndAnnotations" value="1"/>
        </properties>
    </rule>

    <rule ref="SlevomatCodingStandard.Commenting.EmptyComment"/>

    <rule ref="SlevomatCodingStandard.Commenting.ForbiddenAnnotations">
        <properties>
            <property name="forbiddenAnnotations" type="array">
                <element value="@inheritDoc"/>
                <element value="@author"/>
                <element value="@created"/>
                <element value="@version"/>
                <element value="@package"/>
                <element value="@copyright"/>
                <element value="@license"/>
                <element value="@throws"/>
            </property>
        </properties>
    </rule>

    <rule ref="SlevomatCodingStandard.Commenting.InlineDocCommentDeclaration"/>

    <rule ref="SlevomatCodingStandard.Commenting.RequireOneLineDocComment"/>
    <rule ref="SlevomatCodingStandard.Commenting.RequireOneLinePropertyDocComment"/>

    <rule ref="SlevomatCodingStandard.Commenting.UselessFunctionDocComment"/>
    <rule ref="SlevomatCodingStandard.Commenting.UselessInheritDocComment"/>


    <!---->
    <!-- Complexity -->
    <!---->

    <!-- Limits the complexity of functions -->
    <!-- See also https://www.sonarsource.com/docs/CognitiveComplexity.pdf -->
    <rule ref="SlevomatCodingStandard.Complexity.Cognitive">
        <properties>
            <property name="maxComplexity" value="6"/>
        </properties>
    </rule>


    <!---->
    <!-- Control Structures -->
    <!---->

    <!-- Disallow else if in favor of elseif -->
    <!-- Changes type to error, instead of PSR 2's warning. -->
    <rule ref="PSR2.ControlStructures.ElseIfDeclaration.NotAllowed">
        <type>error</type>
    </rule>

    <rule ref="SlevomatCodingStandard.ControlStructures.AssignmentInCondition"/>

    <rule ref="SlevomatCodingStandard.ControlStructures.DisallowContinueWithoutIntegerOperandInSwitch"/>

    <!-- Disallow use of the empty() function -->
    <rule ref="SlevomatCodingStandard.ControlStructures.DisallowEmpty"/>

    <rule ref="SlevomatCodingStandard.ControlStructures.DisallowYodaComparison"/>

    <rule ref="SlevomatCodingStandard.ControlStructures.EarlyExit">
        <properties>
            <property name="ignoreStandaloneIfInScope" value="true"/>
            <property name="ignoreOneLineTrailingIf" value="true"/>
        </properties>
    </rule>

    <!-- Enforces line spacing around return and yield statements -->
    <rule ref="SlevomatCodingStandard.ControlStructures.JumpStatementsSpacing">
        <properties>
            <property name="jumpStatements" type="array">
                <element value="return"/>
                <element value="yield"/>
                <element value="yield_from"/>
            </property>
        </properties>
    </rule>

    <!-- Forbids use of parentheses with require, throw, echo, return, etc. -->
    <rule ref="SlevomatCodingStandard.ControlStructures.LanguageConstructWithParentheses"/>

    <!-- Require parentheses when creating an object with the new keyword -->
    <rule ref="SlevomatCodingStandard.ControlStructures.NewWithParentheses"/>

    <rule ref="SlevomatCodingStandard.ControlStructures.RequireMultiLineCondition">
        <properties>
            <property name="alwaysSplitAllConditionParts" value="true"/>
        </properties>
    </rule>

    <rule ref="SlevomatCodingStandard.ControlStructures.RequireNullCoalesceEqualOperator"/>
    <rule ref="SlevomatCodingStandard.ControlStructures.RequireNullCoalesceOperator"/>
    <rule ref="SlevomatCodingStandard.ControlStructures.RequireNullSafeObjectOperator"/>
    <rule ref="SlevomatCodingStandard.ControlStructures.RequireShortTernaryOperator"/>
    <rule ref="SlevomatCodingStandard.ControlStructures.RequireTernaryOperator"/>
    <rule ref="SlevomatCodingStandard.ControlStructures.UselessIfConditionWithReturn"/>
    <rule ref="SlevomatCodingStandard.ControlStructures.UselessTernaryOperator"/>


    <!---->
    <!-- Exceptions -->
    <!---->

    <!-- Disallows unreachable catch blocks -->
    <rule ref="SlevomatCodingStandard.Exceptions.DeadCatch"/>

    <!-- Enforce use of Throwable instead of Exception -->
    <rule ref="SlevomatCodingStandard.Exceptions.ReferenceThrowableOnly"/>

    <!-- Require a non-capturing catch when the variable is not used -->
    <rule ref="SlevomatCodingStandard.Exceptions.RequireNonCapturingCatch"/>


    <!---->
    <!-- Files -->
    <!---->

    <rule ref="Generic.Files.LineLength">
        <properties>
            <property name="absoluteLineLimit" value="120" />
        </properties>
    </rule>


    <!---->
    <!-- Functions -->
    <!---->

    <rule ref="Generic.PHP.DeprecatedFunctions"/>

    <rule ref="SlevomatCodingStandard.Functions.ArrowFunctionDeclaration">
        <properties>
            <property name="allowMultiLine" value="true"/>
        </properties>
    </rule>
    <rule ref="SlevomatCodingStandard.Functions.DisallowEmptyFunction"/>

    <rule ref="SlevomatCodingStandard.Functions.StrictCall"/>
    <rule ref="SlevomatCodingStandard.Functions.UnusedInheritedVariablePassedToClosure"/>
    <rule ref="SlevomatCodingStandard.Functions.UnusedParameter"/>
    <rule ref="SlevomatCodingStandard.Functions.UselessParameterDefaultValue"/>
    <rule ref="SlevomatCodingStandard.Functions.RequireTrailingCommaInCall"/>
    <rule ref="SlevomatCodingStandard.Functions.RequireTrailingCommaInClosureUse"/>
    <rule ref="SlevomatCodingStandard.Functions.RequireTrailingCommaInDeclaration"/>


    <!---->
    <!-- Namespaces -->
    <!---->

    <rule ref="SlevomatCodingStandard.Namespaces.AlphabeticallySortedUses">
        <properties>
            <property name="caseSensitive" value="false"/>
        </properties>
    </rule>

    <rule ref="SlevomatCodingStandard.Namespaces.DisallowGroupUse"/>
    <rule ref="SlevomatCodingStandard.Namespaces.MultipleUsesPerLine"/>

    <!-- Enforce whitespace usage around namespace declaration -->
    <rule ref="SlevomatCodingStandard.Namespaces.NamespaceDeclaration"/>

    <!-- Enforce newline usage around namespace declaration -->
    <rule ref="SlevomatCodingStandard.Namespaces.NamespaceSpacing"/>

    <rule ref="SlevomatCodingStandard.Namespaces.ReferenceUsedNamesOnly">
        <properties>
            <property name="allowFullyQualifiedExceptions" value="true"/>
            <property name="allowPartialUses" value="true"/>
            <property name="searchAnnotations" value="true"/>
        </properties>
    </rule>

    <!-- Prevent multiple namespace definitions in a file -->
    <rule ref="SlevomatCodingStandard.Namespaces.RequireOneNamespaceInFile"/>

    <rule ref="SlevomatCodingStandard.Namespaces.UnusedUses">
        <properties>
            <!-- Unless they are used in annotations -->
            <property name="searchAnnotations" value="true"/>
        </properties>
    </rule>

    <rule ref="SlevomatCodingStandard.Namespaces.UseDoesNotStartWithBackslash"/>
    <rule ref="SlevomatCodingStandard.Namespaces.UseFromSameNamespace"/>
    <rule ref="SlevomatCodingStandard.Namespaces.UselessAlias"/>
    <rule ref="SlevomatCodingStandard.Namespaces.UseSpacing">
        <properties>
            <property name="linesCountBetweenUseTypes" value="1"/>
        </properties>
    </rule>

    <!---->
    <!-- Operators -->
    <!---->

    <rule ref="Generic.Formatting.SpaceAfterCast">
        <properties>
            <property name="spacing" value="0"/>
        </properties>
    </rule>

    <rule ref="Generic.Formatting.SpaceAfterNot">
        <properties>
            <property name="spacing" value="0"/>
        </properties>
    </rule>

    <!-- Enforce use of === and !== instead of == and != -->
    <rule ref="SlevomatCodingStandard.Operators.DisallowEqualOperators"/>

    <!-- Ensures there are no spaces after the negation operator, "-$x" not "- $x". Does not -->
    <!-- affect binary operations (e.g. "$x - $y") -->
    <rule ref="SlevomatCodingStandard.Operators.NegationOperatorSpacing"/>

    <!-- Enforce use of += and -= -->
    <rule ref="SlevomatCodingStandard.Operators.RequireCombinedAssignmentOperator"/>

    <rule ref="SlevomatCodingStandard.Operators.RequireOnlyStandaloneIncrementAndDecrementOperators"/>
    <rule ref="SlevomatCodingStandard.Operators.SpreadOperatorSpacing"/>


    <!---->
    <!-- PHP -->
    <!---->

    <rule ref="SlevomatCodingStandard.PHP.DisallowDirectMagicInvokeCall"/>
    <rule ref="SlevomatCodingStandard.PHP.DisallowReference"/>

    <!-- Prevent spread operator on PHP functions that are optimized -->
    <rule ref="SlevomatCodingStandard.PHP.OptimizedFunctionsWithoutUnpacking"/>

    <rule ref="SlevomatCodingStandard.PHP.ReferenceSpacing"/>

    <!-- Require use of nowdoc (with single quotes) instead of heredoc where possible -->
    <rule ref="SlevomatCodingStandard.PHP.RequireNowdoc"/>

    <rule ref="SlevomatCodingStandard.PHP.ShortList"/>
    <rule ref="SlevomatCodingStandard.PHP.TypeCast"/>

    <!-- Forbid redundant parens (e.g. `($thing) ? "OKAY!" : "NOPE"`) -->
    <rule ref="SlevomatCodingStandard.PHP.UselessParentheses">
        <properties>
            <!--
                But don't complain if they are actually providing some
                clarification. "Complex" here means the ternary condition
                contains `&&`, `||` etc
            -->
            <property name="ignoreComplexTernaryConditions" value="true"/>
        </properties>
    </rule>

    <rule ref="SlevomatCodingStandard.PHP.UselessSemicolon"/>


    <!---->
    <!-- Type Hints -->
    <!---->

    <!-- Enforce strict types declaration at top of file with two newlines either side -->
    <rule ref="SlevomatCodingStandard.TypeHints.DeclareStrictTypes">
        <properties>
            <property name="linesCountBeforeDeclare" value="1"/>
            <property name="linesCountAfterDeclare" value="1"/>
            <property name="spacesCountAroundEqualsSign" value="0"/>
        </properties>
    </rule>

    <!-- Use generic type hints, "array<int>" instead of array type hints, "array[]" -->
    <rule ref="SlevomatCodingStandard.TypeHints.DisallowArrayTypeHintSyntax"/>

    <!-- Enforce shorthand typehint variables in PHPDocs ("int" instead of "integer") -->
    <rule ref="SlevomatCodingStandard.TypeHints.LongTypeHints"/>

    <!-- Null type hints must be last in PHPDocs -->
    <rule ref="SlevomatCodingStandard.TypeHints.NullTypeHintOnLastPosition"/>

    <!-- Type must be nullable if default value is null -->
    <rule ref="SlevomatCodingStandard.TypeHints.NullableTypeForNullDefaultValue"/>

    <!-- Enforce use of native type hints, removal of irrelevant PHPDoc annotations and require -->
    <!-- the inner  types of traversable types -->
    <rule ref="SlevomatCodingStandard.TypeHints.ParameterTypeHint">
        <exclude name="SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingNativeTypeHint" />
    </rule>
    <rule ref="SlevomatCodingStandard.TypeHints.PropertyTypeHint">
        <exclude name="SlevomatCodingStandard.TypeHints.PropertyTypeHint.MissingNativeTypeHint" />
    </rule>
    <rule ref="SlevomatCodingStandard.TypeHints.ReturnTypeHint"/>

    <!-- There must be a single space between type and variable name ("int $x", not "int$x"), -->
    <!-- and no space between nullability symbol and type ("?int $x" not "? int $x") -->
    <rule ref="SlevomatCodingStandard.TypeHints.ParameterTypeHintSpacing"/>

    <!-- Enforce consistent formatting of return type hints (e.g. "function foo(): ?int") -->
    <rule ref="SlevomatCodingStandard.TypeHints.ReturnTypeHintSpacing"/>

    <!-- Enforce formatting of union type hints -->
    <rule ref="SlevomatCodingStandard.TypeHints.DNFTypeHintFormat">
        <properties>
            <!-- "string|int" not "string | int" -->
            <property name="withSpacesAroundOperators" value="no"/>
            <!-- "?string" not "string|null" -->
            <property name="shortNullable" value="yes"/>
            <!-- "string|int|null" not "null|string|int" -->
            <property name="nullPosition" value="last"/>
        </properties>
    </rule>

    <!-- Forbid using `@var` annotations with constants -->
    <rule ref="SlevomatCodingStandard.TypeHints.UselessConstantTypeHint"/>


    <!---->
    <!-- Statements -->
    <!---->

    <rule ref="Generic.CodeAnalysis.EmptyStatement">
        <exclude name="Generic.CodeAnalysis.EmptyStatement.DetectedCatch"/>
    </rule>


    <!---->
    <!-- Strings -->
    <!---->

    <rule ref="Squiz.Strings.DoubleQuoteUsage.NotRequired"/>

    <rule ref="Generic.Strings.UnnecessaryStringConcat">
        <properties>
            <property name="allowMultiline" value="true"/>
        </properties>
    </rule>


    <!---->
    <!-- Variables -->
    <!---->

    <rule ref="SlevomatCodingStandard.Variables.DisallowSuperGlobalVariable"/>

    <rule ref="SlevomatCodingStandard.Variables.DuplicateAssignmentToVariable"/>

    <rule ref="SlevomatCodingStandard.Variables.UnusedVariable">
        <properties>
            <property name="ignoreUnusedValuesWhenOnlyKeysAreUsedInForeach" value="true"/>
        </properties>
    </rule>

    <rule ref="SlevomatCodingStandard.Variables.UselessVariable" />

    <!---->
    <!-- Whitespace -->
    <!---->

    <rule ref="Squiz.WhiteSpace.SuperfluousWhitespace">
        <properties>
            <!-- Turned on by PSR2, turning back off -->
            <property name="ignoreBlankLines" value="false"/>
        </properties>
    </rule>

    <rule ref="Squiz.WhiteSpace.SuperfluousWhitespace.EmptyLines">
        <severity>error</severity>
    </rule>

    <rule ref="Squiz.WhiteSpace.FunctionSpacing">
        <properties>
            <property name="spacing" value="1" />
            <property name="spacingBeforeFirst" value="0"/>
            <property name="spacingAfterLast" value="0"/>
        </properties>
    </rule>

    <rule ref="SlevomatCodingStandard.Whitespaces.DuplicateSpaces">
        <properties>
            <property name="ignoreSpacesInComment" value="true" />
        </properties>
    </rule>
</ruleset>
