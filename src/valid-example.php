<?php

declare(strict_types=1);

echo 'Hello';

$someCondition = true;
$someOtherCondition = true;

if (
    $someCondition &&
    $someOtherCondition
) {
    return 'Yes';
}

switch ($someCondition) {
    case true:
        echo 'Yes';
        break;
    case false:
        echo 'No';
        break;
    default:
        echo 'Maybe';
        break;
}

// I should now be able to make lines which have a much longer length - now 120 characters up from just 100 characters

// We should also be able to use multiline arrow functions that aren't static
$function = fn ()
    =>
    'Hello';

$function();
