### 3.0.2 [2025-02-28]
- Pin slevomat/coding-standard to v8.16

### 3.0.1 [2024-05-21]

- Increase line length limit to 120
- Remove requirement for closures to be static
- Remove requirement for native types where this could result in a PHP error
- Allow multi-line arrow functions

### 3.0.0 [2024-01-10]

- Require trailing commas where possible (#11)
- Run composer update and fix incompatible rules (#14)

### 2.0.1 [2022-11-07]

-  Allow constructor property promotion
-  Change character limit to be absolutely 100 character
-  Adjust warning and error levels

### 2.0.0 [2022-10-18]

-  Update coding standards to support PHP 8.1 (#9)

### 1.1.3 [2022-02-02]

-  Update composer.json to allow package to be installed when running PHP 8 

### 1.1.2 [2022-01-28]

-  Released in error

### 1.1.1 [2021-10-20]

- Remove annotation ordering check (#7)

### 1.1.0 [2021-09-28]

- Remove PHPStan, PHP Parallel Lint, and pre-push script
- Fix 'incorrect annotations group' (#6)
